set -e

function git-clone
{
    name=$1
    shift
    repo=$1
    shift

    srcdir="`pwd`/src"

    cloned=0
    if ! [ -d "$srcdir"/$name ] ; then
        mkdir -p "$srcdir"
        pushd "$srcdir"
        git clone --depth 1 $repo --single-branch $GIT_EXTRA
        cloned=1
        popd
    fi

    if [[ ! -z "$PATCH" ]]; then
        pushd "$srcdir"/$name
        echo "applying patch $PATCH"
        curl $PATCH | git apply -
        popd
    fi
}
